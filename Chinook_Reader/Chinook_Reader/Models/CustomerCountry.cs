﻿namespace Chinook_Reader.Models
{
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int CountryAmount { get; set; }
    }
}
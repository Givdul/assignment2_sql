﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_Reader.Models
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string TopGenre { get; set; }
        public int GenreAmount { get; set; }
    }
}

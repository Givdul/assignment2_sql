﻿namespace Chinook_Reader.Models
{
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public decimal Total { get; set; }
    }
}
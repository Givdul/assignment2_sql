﻿using System.Collections.Generic;
using Chinook_Reader.Models;

namespace Chinook_Reader.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(string id);
        public List<Customer> GetAllCustomers();
        public Customer GetCustomerByName(string name);
        public List<Customer> GetPageOfCustomers(int limit, int offset);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerCountry> GetAllCountries();
        public List<CustomerSpender> GetAllSpenders();
        public List<CustomerGenre> GetCustomerTopGenre(string id);
    }
}
﻿using System;
using System.Collections.Generic;
using Chinook_Reader.Models;
using Microsoft.Data.SqlClient;

namespace Chinook_Reader.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Gets a specific customers information from the Chinook database based on their CustomerId.
        /// Uses an SQL query to get the data and an SQL Connectionstring to connect to the the database.
        /// </summary>
        /// <param name="id">The CustomerId</param>
        /// <returns>A string of all the information we queried</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public Customer GetCustomer(string id)
        {
            Customer customer = new Customer();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                         " Where CustomerId = @CustomerId";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return customer;
        }

        /// <summary>
        /// Gets all the customers in the database with the specified fields.
        /// Uses a SQL query to get the data and an SQLconnection string to connect to the database.
        /// </summary>
        /// <returns>Every customer and their data as strings</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                if(!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);
                                if(!reader.IsDBNull(5)) customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return customers;
        }

        /// <summary>
        /// Gets a specific customers information called by their Name (First + Last) in the database with the specified fields.
        /// Uses a SQL query to get the data and an SQLconnection string to connect to the database.
        /// </summary>
        /// <returns>A specific customers data as string</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public Customer GetCustomerByName(string name)
        {
            Customer customer = new Customer();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                         $" Where FirstName + ' ' + LastName LIKE @CustomerName";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerName", $"%{name}%");
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return customer;
        }
        /// <summary>
        /// Gets a page of customers information from the database.
        /// Uses an Sql query and two paramaterers to get a specific page of customers.
        /// Uses SQLconnection string to connect to the database.
        /// </summary>
        /// <param name="limit">Amount of customers</param>
        /// <param name="offset">How far from 0 you wanna start to print the amount</param>
        /// <returns>The information of the customers based on the amount set</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public List<Customer> GetPageOfCustomers(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                         " ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return customers;
        }
        /// <summary>
        /// Adds a new customer to the database using SQL query
        /// </summary>
        /// <param name="customer">Customer object with all information needed.</param>
        /// <returns>Returns true or false if the customer was succesfully added or not</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone , Email)" + 
                         " VALUES(@FirstName, @Lastname, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false; // If rows affected more than 0 return true, else return false.
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }
        /// <summary>
        /// Updates values for a customer already in the database using SQL query.
        /// There is functionality so you dont have to add all values when updating and the method wont write send null as a value for fields not set.
        /// </summary>
        /// <param name="customer">Customer object with values you want to change</param>
        /// <returns>Returns true or false depending on if the update was succesful or not</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = IsNull(@FirstName, FirstName), LastName = IsNull(@LastName, LastName), " +
                         "Country = IsNull(@Country, Country), PostalCode = IsNull(@PostalCode, PostalCode), " +
                         "Phone = IsNull(@Phone, Phone), Email = IsNull(@Email, Email) " +
                         "WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", $"{customer.FirstName}");
                        cmd.Parameters.AddWithValue("@LastName", $"{customer.LastName}");
                        cmd.Parameters.AddWithValue("@Country", $"{customer.Country}");
                        cmd.Parameters.AddWithValue("@PostalCode", $"{customer.PostalCode}");
                        cmd.Parameters.AddWithValue("@Phone", $"{customer.Phone}");
                        cmd.Parameters.AddWithValue("@Email", $"{customer.Email}");
                        success = cmd.ExecuteNonQuery() > 0 ? true : false; // If rows affected more than 0 return true, else return false.
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }
        /// <summary>
        /// Gets all the countries a customer has in the database and sorts them by amount in a descending order (highest to lowest).
        /// </summary>
        /// <returns>List of countries based on their customer amount in a descending order</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public List<CustomerCountry> GetAllCountries()
        {
            List<CustomerCountry> countryList = new List<CustomerCountry>();
            string sql = "SELECT country as country, count(country) as CountryAmount FROM Customer GROUP BY country ORDER BY CountryAmount DESC; ";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry cc = new CustomerCountry();
                                cc.Country = reader.GetString(0);
                                cc.CountryAmount = reader.GetInt32(1);
                                countryList.Add(cc);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return countryList;
        }
        /// <summary>
        /// Gets the amount a customer has spent on songs in total from the invoice table.
        /// Sorts by Amount in descending order.
        /// </summary>
        /// <returns>List of customers total spendings sorted in a descending order</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public List<CustomerSpender> GetAllSpenders()
        {
            List<CustomerSpender> totalSpending = new List<CustomerSpender>();
            string sql = "SELECT CustomerId, SUM(Total) FROM Invoice GROUP BY CustomerId ORDER BY SUM(Total) DESC; ";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender cs = new CustomerSpender();
                                cs.CustomerId = reader.GetInt32(0);
                                cs.Total = reader.GetDecimal(1);
                                totalSpending.Add(cs);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return totalSpending;
        }
        /// <summary>
        /// Gets a specific users top genre.
        /// Checks their purchases and finds the most amount of tracks in a specific genre.
        /// Finds their top 3 favourite genres incase of ties.
        /// </summary>
        /// <param name="id">CustomerId</param>
        /// <returns>List of genres and amount of songs purchased per genre sorted in descending order</returns>
        /// <exception cref="SqlException">Throw if Sql encounters any errors with the query or connection</exception>
        public List<CustomerGenre> GetCustomerTopGenre(string id)
        {
            List<CustomerGenre> genreList = new List<CustomerGenre>();

            string sql = "SELECT TOP 3 Customer.CustomerId, genre.name, COUNT(genre.name) as genreAmount FROM Invoice INNER JOIN InvoiceLine" +
                " ON Invoice.InvoiceId = InvoiceLine.InvoiceId LEFT JOIN Customer On Invoice.CustomerId = Customer.CustomerId" +
                " LEFT JOIN Track ON InvoiceLine.TrackId = track.TrackId LEFT JOIN Genre ON track.GenreId = genre.GenreId" +
                " WHERE Customer.CustomerId = @CustomerId GROUP BY Customer.CustomerId, genre.name ORDER BY genreAmount DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre customer = new CustomerGenre();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.TopGenre = reader.GetString(1);
                                customer.GenreAmount = reader.GetInt32(2);
                                genreList.Add(customer);
                                
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return genreList;
        }
    }
}
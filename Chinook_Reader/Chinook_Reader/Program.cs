﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chinook_Reader.Models;
using Chinook_Reader.Repositories;

namespace Chinook_Reader
{
    public class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repo = new CustomerRepository();
            
            TestSelectCustomerId(repo);
            TestSelectAllCustomers(repo);
            TestSelectCustomerName(repo);
            TestSelectPageOfCustomers(repo);
            TestAddCustomer(repo);
            TestUpdateCustomer(repo);
            TestGetAllCountries(repo);
            TestGetAllSpenders(repo);
            TestGetCustomerTopGenre(repo);
        }
        //Methods that call on the SQL methods so we can test them
        static void TestSelectCustomerId(ICustomerRepository repo)
        {
            PrintCustomer(repo.GetCustomer("1"));
        }

        static void TestSelectAllCustomers(ICustomerRepository repo)
        {
            PrintAllCustomers(repo.GetAllCustomers());
        }

        static void TestSelectCustomerName(ICustomerRepository repo)
        {
            PrintCustomer(repo.GetCustomerByName("bjørn"));
        }

        static void TestSelectPageOfCustomers(ICustomerRepository repo)
        {
            PrintAllCustomers(repo.GetPageOfCustomers(10, 10));
        }
        static void TestGetAllCountries(ICustomerRepository repo)
        {
            PrintAllCountries(repo.GetAllCountries());
        }
        static void TestGetAllSpenders(ICustomerRepository repo)
        {
            PrintAllSpenders(repo.GetAllSpenders());
        }
        static void TestGetCustomerTopGenre(ICustomerRepository repo)
        {
            PrintTopGenre(repo.GetCustomerTopGenre("12"));
        }
        /// <summary>
        /// Creates a Customer object and defines values for fields.
        /// And Adds it to the database
        /// </summary>
        /// <param name="repo">Customer interface</param>
        static void TestAddCustomer(ICustomerRepository repo)
        {
            Customer customer = new Customer();
            customer.FirstName = "Martin";
            customer.LastName = "Kanestrøm";
            customer.Country = "Norway";
            customer.PostalCode = "2150";
            customer.Phone = "40074554";
            customer.Email = "martin.kanestrom@no.experis.com";

            bool success = repo.AddNewCustomer(customer);
            Console.WriteLine(success);
        }
        /// <summary>
        /// Updates the fields of an already existing customer in the database.
        /// </summary>
        /// <param name="repo">Customer Interface</param>
        static void TestUpdateCustomer(ICustomerRepository repo)
        {
            Customer customer = new Customer();
            customer.CustomerId = 11;
            customer.FirstName = "Martin";
            customer.LastName = "Kanestrøm";
            customer.Country = "Norway";
            customer.PostalCode = "2150";

            bool success = repo.UpdateCustomer(customer);
            Console.WriteLine(success);
        }
        /// <summary>
        /// Prints a specific customers information to the console.
        /// </summary>
        /// <param name="customer">Customer object</param>
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"Id: {customer.CustomerId} | Name: {customer.FirstName} {customer.LastName} | Postal code: {customer.PostalCode} | Phone: {customer.Phone} | Email: {customer.Email}");
        }
        /// <summary>
        /// Prints all customers in the database to the console.
        /// </summary>
        /// <param name="customers">Customers Enumerable</param>
        static void PrintAllCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        /// <summary>
        /// Prints to the console list of countries customers are registred as in descending order per country.
        /// </summary>
        /// <param name="customerCountries">Countries customer amount Enumerable</param>
        static void PrintAllCountries(IEnumerable<CustomerCountry> customerCountries)
        {
            foreach (CustomerCountry country in customerCountries)
            {
                Console.WriteLine($"Country: {country.Country} \t| Amount: {country.CountryAmount}");
            }
        }
        /// <summary>
        /// Prints to the console all customers total spent amount on tracks sorted in a descending order.
        /// </summary>
        /// <param name="customerSpenders">Customers total spending amount Enumerable</param>
        static void PrintAllSpenders(IEnumerable<CustomerSpender> customerSpenders)
        {
            foreach (CustomerSpender spendings in customerSpenders)
            {
                Console.WriteLine($"Customer: {spendings.CustomerId} \t| Total: {spendings.Total}");
            }
        }
        /// <summary>
        /// Prints a specific customers favorite genre based on tracks purchased to the console.
        /// Checks for ties. If a tie exists print them all.
        /// </summary>
        /// <param name="customerGenre">CustomersGenre Enumerable</param>
        static void PrintTopGenre(IEnumerable<CustomerGenre> customerGenre)
        {
            {
                foreach (CustomerGenre genre in customerGenre)
                {
                    if (genre.GenreAmount == customerGenre.ElementAtOrDefault(0).GenreAmount)
                        Console.WriteLine($"Customer: {genre.CustomerId} \t| Genre: {genre.TopGenre}\t| Amount: {genre.GenreAmount}");
                }
            }
        }
    }
}

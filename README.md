# Assignment 2: Create a database and access it

SQL Scripts folder contains all the scripts from Appendix A.

Chinook_Reader folder contains the VS project where we use .NET to read and manipulate the Chinook database.

## Usage
**Note:** You need to change the datasource in *ConnectionHelper.cs* to your own SSMS server name. 

```c#
// initialize CustomerRepository interface
ICustomerRepository repo = new CustomerRepository();

//  read a specific customer from the database by id
repo.GetCustomer("1")

// read all customers in the database
repo.GetAllCustomer()

// read customer by their name
repo.GetCustomerByName("bjørn")

// return a page of customers, takes offset and limit as parameters
repo.GetPageOfCustomers(10,10)

// add a new customer to the database, takes a Customer object as parameter
repo.AddNewCustomer(customer)

// update an existing customer, takes a Customer object as parameter
repo.UpdateCustomer(customer)

// return number of customers per country in a descending order
repo.GetAllCountries()

// return total amount spent on songs per customer in a descending order
repo.GetAllSpenders()

// return a specific customers favourite genre based on their purchases, takes CustomerId as a parameter
repo.GetcustomerTopGenre("12")

```
## Authors
Ludvig Hansen and Martin Kanestrøm

## License
[MIT](https://choosealicense.com/licenses/mit/)

USE SuperheroesDb;

INSERT INTO Power (Name, Description)
VALUES ('Flight', 'Can fly'),
('Super Strength', 'Has inhumane strength'),
('Super Rich', 'Is a Multi-Billionare'),
('Super Senses', 'Has super senses'),
('Shoot Webs', 'Can shoot spider webs');

INSERT INTO SuperHeroPowers (Superhero_Id, Power_Id)
VALUES (1, 1),
(1, 2),
(3, 2),
(2, 2),
(1, 4),
(3, 4),
(3, 5);
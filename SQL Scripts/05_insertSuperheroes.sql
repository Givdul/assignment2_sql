USE SuperheroesDb;

INSERT INTO SuperHero (Name, Alias, Origin)
VALUES ('Superman', 'Clark Kent', 'Krypton'),
('Batman', 'Bryce Wayne', 'Gotham'),
('Spiderman','Peter Parker','Queens');

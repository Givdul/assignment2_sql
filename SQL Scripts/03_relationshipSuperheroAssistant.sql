USE SuperheroesDb;

ALTER TABLE Assistant
ADD Superhero_Id int;
ALTER TABLE Assistant
ADD CONSTRAINT fk_Superhero_Assistant
FOREIGN KEY (Superhero_Id) REFERENCES Superhero(Id);
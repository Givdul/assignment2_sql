USE SuperheroesDb;

CREATE TABLE SuperHeroPowers (
	Superhero_Id INT,
	Power_Id INT,
	CONSTRAINT fk_SuperheroPowers_Superhero FOREIGN KEY (Superhero_Id) REFERENCES Superhero(Id),
	CONSTRAINT fk_SuperheroPower_Power FOREIGN KEY (Power_Id) REFERENCES Power(Id)
);